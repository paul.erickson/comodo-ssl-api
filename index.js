const WSDL = 'https://cert-manager.com/ws/EPKIManagerSSL?wsdl';
const SOAP = require('soap');
SOAP.WSDL.prototype.ignoredNamespaces = ['xs', 'xsd'];

// Configure Certificate Type Map.
var certTypeMap = new Map();
certTypeMap.set(224, { id: 224, name: 'InCommon SSL (SHA-2)', terms: [ 1, 2, 3 ] });
certTypeMap.set(227, { id: 227, name: 'InCommon Wildcard SSL Certificate (SHA-2)', terms: [ 1, 2, 3 ] });
certTypeMap.set(226, { id: 226, name: 'InCommon Multi Domain SSL (SHA-2)', terms: [ 1, 2, 3 ] });
certTypeMap.set(228, { id: 228, name: 'InCommon Unified Communications Certificate (SHA-2)', terms: [ 1, 2, 3 ] });
certTypeMap.set(229, { id: 229, name: 'Comodo EV Multi Domain SSL (SHA-2)', terms: [ 1, 2 ] });
certTypeMap.set(98,  { id: 98,  name: 'Comodo EV Multi Domain SSL', terms: [ 1, 2 ] });
certTypeMap.set(215, { id: 215, name: 'IGTF Server Cert', terms: [ 1, 1 ] });
certTypeMap.set(283, { id: 283, name: 'IGTF Multi Domain', terms: [ 1, 1 ] });
certTypeMap.set(179, { id: 179, name: 'AMT SSL Certificate', terms: [ 1, 2, 3 ] });
certTypeMap.set(180, { id: 180, name: 'AMT Wildcard SSL Certificate', terms: [ 1, 2, 3 ] });
certTypeMap.set(181, { id: 181, name: 'AMT Multi-Domain SSL Certificate', terms: [ 1, 2, 3 ] });
certTypeMap.set(243, { id: 243, name: 'Comodo Elite SSL Certificate (FileMaker) (SHA-2)', terms: [ 1, 2, 3 ] });
certTypeMap.set(284, { id: 284, name: 'Incommon ECC', terms: [ 1, 2, 3 ] });
certTypeMap.set(286, { id: 286, name: 'Incommon ECC Multi Domain', terms: [ 1, 2, 3 ] });
certTypeMap.set(285, { id: 285, name: 'Incommon ECC Wildcard', terms: [ 1, 2, 3 ] });
certTypeMap.set(60,  { id: 60,  name: 'Comodo EV SSL Certificate', terms: [ 1, 2 ] });
certTypeMap.set(249, { id: 249, name: 'Comodo EV SSL Certificate (SHA-2)', terms: [ 1, 2 ] });

// Configure Server Type Map.
var serverTypeMap = new Map();
serverTypeMap.set(1,  { id: 1,  name: 'AOL' });
serverTypeMap.set(2,  { id: 2,  name: 'Apache/ModSSL' });
serverTypeMap.set(3,  { id: 3,  name: 'Apache-SSL (Ben-SSL, not Stronghold)' });
serverTypeMap.set(4,  { id: 4,  name: 'C2Net Stronghold' });
serverTypeMap.set(33, { id: 33, name: 'Cisco 3000 Series VPN Concentrato' });
serverTypeMap.set(34, { id: 34, name: 'Citrix' });
serverTypeMap.set(5,  { id: 5,  name: 'Cobalt Raq' });
serverTypeMap.set(6,  { id: 6,  name: 'Covalent Server Software' });
serverTypeMap.set(7,  { id: 7,  name: 'IBM HTTP Server' });
serverTypeMap.set(8,  { id: 8,  name: 'IBM Internet Connection Server' });
serverTypeMap.set(9,  { id: 9,  name: 'iPlanet' });
serverTypeMap.set(10, { id: 10, name: 'Java Web Server (Javasoft / Sun)' });
serverTypeMap.set(11, { id: 11, name: 'Lotus Domino' });
serverTypeMap.set(12, { id: 12, name: 'Lotus Domino Go!' });
serverTypeMap.set(13, { id: 13, name: 'Microsoft IIS 1.x to 4.x' });
serverTypeMap.set(14, { id: 14, name: 'Microsoft IIS 5.x and later' });
serverTypeMap.set(15, { id: 15, name: 'Netscape Enterprise Server' });
serverTypeMap.set(16, { id: 16, name: 'Netscape FastTrac' });
serverTypeMap.set(17, { id: 17, name: 'Novell Web Server' });
serverTypeMap.set(18, { id: 18, name: 'Oracle' });
serverTypeMap.set(19, { id: 19, name: 'Quid Pro Quo' });
serverTypeMap.set(20, { id: 20, name: 'R3 SSL Server' });
serverTypeMap.set(21, { id: 21, name: 'Raven SSL' });
serverTypeMap.set(22, { id: 22, name: 'RedHat Linux' });
serverTypeMap.set(23, { id: 23, name: 'SAP Web Application Server' });
serverTypeMap.set(24, { id: 24, name: 'Tomcat' });
serverTypeMap.set(25, { id: 25, name: 'Website Professional' });
serverTypeMap.set(26, { id: 26, name: 'WebStar 4.x and later' });
serverTypeMap.set(27, { id: 27, name: 'WebTen (from Tenon)' });
serverTypeMap.set(28, { id: 28, name: 'Zeus Web Server' });
serverTypeMap.set(29, { id: 29, name: 'Ensim' });
serverTypeMap.set(30, { id: 30, name: 'Plesk' });
serverTypeMap.set(31, { id: 31, name: 'WHM/cPanel' });
serverTypeMap.set(32, { id: 32, name: 'H-Sphere' });
serverTypeMap.set(-1, { id: -1, name: 'OTHER' });

// Map for Return Codes for each SOAP call.
var enrollReturnMap = new Map();
enrollReturnMap.set(-3, 'The username argument is invalid');
enrollReturnMap.set(-7, 'Country is not a valid ISO-3155 country');
enrollReturnMap.set(-9, 'The CSR is not valid Base-64 data');
enrollReturnMap.set(-10, 'The CSR cannot be decoded');
enrollReturnMap.set(-11, 'The CSR uses an unsupported algorithm');
enrollReturnMap.set(-12, 'The CSR has an invalid signature');
enrollReturnMap.set(-13, 'The CSR uses an unsupported key size');
enrollReturnMap.set(-14, 'An unknown error occurred');
enrollReturnMap.set(-16, 'Permission denied');
enrollReturnMap.set(-31, 'The email address is not valid');
enrollReturnMap.set(-32, 'The revocation phrase is missing');
enrollReturnMap.set(-33, 'The certificate type is invalid');
enrollReturnMap.set(-34, 'The secret key is invalid');
enrollReturnMap.set(-35, 'The server type is invalid');
enrollReturnMap.set(-36, 'The term is invalid for the certificate type');
enrollReturnMap.set(-100, 'Invalid authentication data for the customer');
enrollReturnMap.set(-101, 'Invalid authentication data for the organization');
enrollReturnMap.set(-110, 'Domain is not allowed for customer');
enrollReturnMap.set(-111, 'Domain is not allowed for the organization');
enrollReturnMap.set(-120, 'Customer configuration does not allow this action');

var getCollectStatusReturnMap = new Map();
getCollectStatusReturnMap.set(1, 'Certificate available');
getCollectStatusReturnMap.set(0, 'Certificate being processed by Comodo');
getCollectStatusReturnMap.set(-6, 'Admin has declined request');
getCollectStatusReturnMap.set(-21, 'Certificate revoked');
getCollectStatusReturnMap.set(-23, 'Waiting for approval by admin');
getCollectStatusReturnMap.set(-14, 'An unknown error occurred');
getCollectStatusReturnMap.set(-16, 'Permission denied');
getCollectStatusReturnMap.set(-40, 'Invalid ID');
getCollectStatusReturnMap.set(-100, 'Invalid authentication data for the customer');
getCollectStatusReturnMap.set(-101, 'Invalid authentication data for the organization');
getCollectStatusReturnMap.set(-110, 'Domain is not allowed for customer');
getCollectStatusReturnMap.set(-111, 'Domain is not allowed for the organization');
getCollectStatusReturnMap.set(-120, 'Customer configuration does not allow this action');

var collectReturnMap = new Map();
collectReturnMap.set(0, 'Certificate being processed by Comodo');
collectReturnMap.set(1, 'Certificate available');
collectReturnMap.set(2, 'Certificate attached');
collectReturnMap.set(-14, 'An unknown error occurred');
collectReturnMap.set(-16, 'Permission denied');
collectReturnMap.set(-20, 'The certificate request has been rejected');
collectReturnMap.set(-21, 'The certificate has been revoked');
collectReturnMap.set(-22, 'Awaiting payment');
collectReturnMap.set(-100, 'Invalid authentication data for the customer');
collectReturnMap.set(-101, 'Invalid authentication data for the organization');
collectReturnMap.set(-110, 'Domain is not allowed for customer');
collectReturnMap.set(-111, 'Domain is not allowed for the organization');
collectReturnMap.set(-120, 'Customer configuration does not allow this action');

var renewReturnMap = new Map();
renewReturnMap.set(-3, 'Internal error');
renewReturnMap.set(-4, 'Invalid renewId');

var collectRenewedReturnMap = new Map();
collectRenewedReturnMap.set(0, 'Issued');
collectRenewedReturnMap.set(-1, 'Applied');
collectRenewedReturnMap.set(-2, 'Certificate error, invalid state');
collectRenewedReturnMap.set(-3, 'Internal error');
collectRenewedReturnMap.set(-4, 'SSL certificate does not exist');
collectRenewedReturnMap.set(-5, 'Waiting for approval by admin');
collectRenewedReturnMap.set(-6, 'Admin has declined request');

var revokeReturnMap = new Map();
revokeReturnMap.set(0, 'Successful');
revokeReturnMap.set(-14, 'An unknown error occurred');
revokeReturnMap.set(-16, 'Permission denied');
revokeReturnMap.set(-100, 'Invalid authentication data for the customer');
revokeReturnMap.set(-101, 'Invalid authentication data for the organization');
revokeReturnMap.set(-110, 'Domain is not allowed for customer');
revokeReturnMap.set(-111, 'Domain is not allowed for the organization');
revokeReturnMap.set(-120, 'Customer configuration does not allow this action');

var updateRequesterExtReturnMap = new Map();
updateRequesterExtReturnMap.set(0, 'Successful');
updateRequesterExtReturnMap.set(-14, 'An unknown error occurred');
updateRequesterExtReturnMap.set(-16, 'Permission denied');
updateRequesterExtReturnMap.set(-31, 'External requester is invalid');
updateRequesterExtReturnMap.set(-100, 'Invalid authentication data for the customer');
updateRequesterExtReturnMap.set(-101, 'Invalid authentication data for the organization');
updateRequesterExtReturnMap.set(-106, 'EULA is not accepted');
updateRequesterExtReturnMap.set(-110, 'Domain is not allowed for customer');
updateRequesterExtReturnMap.set(-111, 'Domain is not allowed for the organization');
updateRequesterExtReturnMap.set(-112, 'KU/EKU template is not allowed for the customer');
updateRequesterExtReturnMap.set(-113, 'KU/EKU tempalte is not allowed');
updateRequesterExtReturnMap.set(-114, 'Client Cert Type is not available for the organization');
updateRequesterExtReturnMap.set(-115, 'Domain is not DCV validated');
updateRequesterExtReturnMap.set(-120, 'Customer configuration does not allow this action');

var getCustomerCertTypesReturnMap = new Map();
getCustomerCertTypesReturnMap.set(0, 'Successful');
getCustomerCertTypesReturnMap.set(-14, 'An unknown error occurred');
getCustomerCertTypesReturnMap.set(-16, 'Permission denied');

var formatTypesMap = new Map();
formatTypesMap.set(0, 'X509 PEM Bundle');
formatTypesMap.set(1, 'X509 PEM Certificate Only');
formatTypesMap.set(2, 'X509 PEM Intermediate Certificate Only');
formatTypesMap.set(3, 'PKCS#7 PEM Bundle');
formatTypesMap.set(4, 'PKCS#7 DER Bundle');

module.exports = function(customerLoginUri, login, password, orgId, secretKey) {
	// Check the provided data for consistency.
	// The orgId must be a positive integer.
	if (!(typeof(orgId) == 'number')) {
		throw new TypeError("Argument 'orgId' must be a number.");
	}
	if (isNaN(orgId) || orgId < 0) {
		throw new TypeError("Argument 'orgId' must be a positive number.");
	}
	// The secretKey is a string that cannot be more than 20 characters long.
	if (!(typeof(secretKey) == 'string')) {
		throw new TypeError("Argument 'secretKey' must be a string.");
	}
	if (secretKey.length>20) {
		throw new TypeError("Argument 'secretKey' is limited to 20 characters.");
	}

	this.authData = {'customerLoginUri': customerLoginUri, 'login': login, 'password': password};
	this.orgId = orgId;
	this.secretKey = secretKey;
	this.certTypeMap = certTypeMap;
	this.serverTypeMap = serverTypeMap;

	/**
	 * Calls the getCustomerCertTypesByOrg soap command.
	 * This command returns a list of available Certificate Types for the
	 * provided orgId.
	 */
	this.getCustomerCertTypesByOrg = function(orgId, callback, timeout = 0) {
		// The orgId must be a positive integer.
		if (!(typeof(orgId) == 'number')) {
			throw new TypeError("The argument 'orgId' must be a number.");
		}
		if (isNaN(orgId) || (orgId < 0)) {
			throw new TypeError("Argument 'orgId' must be a positive integer.");
		}

		var params = {authData: this.authData, orgId: orgId};
		SOAP.createClient(WSDL, function(err, client) {
		    client.EPKIManagerSSLService.EPKIManagerSSLPort.getCustomerCertTypesByOrg(params, function(err, result, raw, soapHeader) {
				if (err) {
					callback(err, null);
				} else {
					var returnValue = result['return']['statusCode'];
					if (returnValue >= 0) {
						callback(null, result['return']['types']);
					} else {
						var errorMessage = getCustomerCertTypesReturnMap.get(returnValue);
						var errorCode = returnValue;
						callback(new Error(errorMessage, errorCode));
					}
				}
		    }, {timeout: timeout});
		});
	}

	/**
	 * Calls the getCustomerCertTypes soap command.
	 * This command restursn a list of available Certificate Types
	 */
	this.getCustomerCertTypes = function(callback, timeout = 0) {
		var params = {authData: this.authData };
		SOAP.createClient(WSDL, function(err, client) {
			client.EPKIManagerSSLService.EPKIManagerSSLPort.getCustomerCertTypes(params, function(err, result, raw, soapHeaders) {
				if (err) {
					callback(err, null);
				} else {
					var returnValue = result['return']['statusCode'];
					if (returnValue >= 0) {
						callback(null, result['return']['types']);
					} else {
						var errorMessage = getCustomerCertTypesReturnMap.get(returnValue);
						var errorCode = returnValue;
						callback(new Error(errorMessage, errorCode));
					}
				}
			}, {timeout: timeout});
		});
	}

	/**
	 * Calls the enroll soap command.
	 * This command adds a new certificate to the certificate manager.
	 * (string)	csr				A valid CSR.
	 * (string)	phrase			The revocation passphrase, cannot be empty or
	 *							null.
	 * (array)	subjAltNames	A comma seperated list of domain names.
	 * (int)	certType		The ID of the Certificate Type.
	 * (int)	numberServers	The number of servers the certificate will be
	 *							used on.
	 * (int)	serverType		The ID of the Server Type.
	 * (int)	term			The length the certificate will be valid.
	 * (string)	comments		Comments to attach to the certificate request.
	 * callback(err, result)	If there was an error, it will be contained in
 	 *							err.  Result will be a negative number on
	 *							failure and the ID of the certificate on
	 *							success.
	 */
	this.enroll = function(csr, phrase, subjAltNames, certType, numberServers, serverType, term, comments, callback, timeout = 0) {
		// The csr must be a string and cannot be null or empty.
		if (!(typeof(csr) == 'string')) {
			throw new TypeError("The argument 'csr' must be a string.");
		}
		if ((csr == null) || (csr.length == 0)) {
			throw new TypeError("The argument 'csr' cannot be empty.");
		}
		// The phrase must be a string and cannot be null or empty.
		if (!(typeof(phrase) == 'string')) {
			throw new TypeError("The argument 'phrase' must be a string.");
		}
		if ((phrase == null) || (phrase.length == 0)) {
			throw new TypeError("The argument 'phrase' cannot be empty.");
		}
		// The subjAltNames can be an array or null.  The array items must be
		// valid fully qualified domain names.
		// ****FIXME****
		// The certType must be a number corresponding to a known
		// certificateType.
		if (!(typeof(certType) == 'number')) {
			throw new TypeError("The argument 'certType' must be a number.");
		}
		if (!certTypeMap.has(certType)) {
			throw new TypeError("The argument 'certType' must correspond to a valid Certificate Type in CERT_TYPE.");
		}
		// The numberServers must be a positive integer.
		if (!(typeof(numberServers) == 'number')) {
			throw new TypeError("The argument 'numberServers' must be a number.");
		}
		if (isNaN(numberServers) || (numberServers < 0)) {
			throw new TypeError("Argument 'numberServers' must be a positive integer.");
		}
		// The serverType must be a number corresponding to known Server Type.
		if (!(typeof(serverType) == 'number')) {
			throw new TypeError("Argument 'serverType' must be a number.");
		}
		if (!serverTypeMap.has(serverType)) {
			throw new TypeError("Argument 'serverType' must correspond to a valid Server Type in SERVER_TYPE.");
		}
		// The term must be a number corresponding to a term available for the
		// selected Certificate Type.
		if (!(typeof(term) == 'number')) {
			throw new TypeError("Argument 'term' must be a number.");
		}
		if (!(certTypeMap.get(certType).terms.indexOf(term) >= 0)) {
			throw new TypeError("Argument 'term' must be a valid term for the provided Certificate Type.");
		}
		// The comments must be a string.
		if (!(typeof(comments) == 'string')) {
			throw new TypeError("Argument 'comments' must be a string.");
		}
		// The callback must be a function.
		if (!(typeof(callback) == 'function')) {
			throw new TypeError("Argument 'callback' must be a function.");
		}

		var params = {
			authData: this.authData,
			orgId: this.orgId,
			secretKey: this.secretKey,
			csr: csr,
			phrase: phrase,
			subjAltNames: subjAltNames,
			certType: certTypeMap.get(certType),
			numberServers: numberServers,
			serverType: serverType,
			term: term,
			comments: comments
		};
		SOAP.createClient(WSDL, function(err, client) {
			client.EPKIManagerSSLService.EPKIManagerSSLPort.enroll(params, function(err, result, raw, soapHeader) {
				var returnValue = result['return'];
				if (err) {
					callback(err, null);
				} else {
					var returnValue = result['return'];
					if (returnValue >= 0) {
						callback(null, returnValue);
					} else {
						var errorMessage = enrollReturnMap.get(returnValue);
						var errorCode = returnValue;
						callback(new Error(errorMessage, errorCode));
					}
				}
			}, {timeout: timeout});
		});
	}

	/**
	 * Calls the updateRequesterExt soap command.
	 * This command updates the list of External Requesters.
	 * (int)	id				The ID of the certificate to update.
	 * (array)	requesterExt	An array of email addresses.
	 * callback(err, result)	If there was an error, it will be contained in
	 *							err.  Result will contain the result of the
	 *							operation from the webservice.  0 for success,
	 *							a negative integer for failure.
	 */
	this.updateRequesterExt = function(id, requesterExt, callback, timeout = 0) {
		// The id must be a positive number.
		if (!(typeof(id) == 'number')) {
			throw new TypeError("The argument 'id' must be a number.");
		}
		if (isNaN(id) || (id <= 0)) {
			throw new TypeError("The argument 'id' must be a positive number.");
		}
		// The requesterExt must be an array of email addresses.
		// ****FIXME****
		// The callback must be a function.
		if (!(typeof(callback) == 'function')) {
			throw new TypeError("The argument 'callback' must be a function.");
		}

		var params = {
			authData: this.authData,
			id: id,
			requesterExt: requesterExt
		};

		SOAP.createClient(WSDL, function(err, client) {
			client.updateRequesterExt(params, function(err, result, raw, soapHeader) {
				if (err) {
					callback(err, null);
				} else {
					var returnValue = result['return'];
					if (returnValue >= 0) {
						callback(null, returnValue);
					} else {
						var errorMessage = updateRequesterExtReturnMap.get(returnValue);
						var errorCode = returnValue;
						callback(new Error(errorMessage, returnValue));
					}
				}
			}, {timeout: timeout});
		});
	}

	/**
	 * Calls the getCollectStatus SOAP command.
	 * This command gets the status of the certificate identified by the
	 * provided ID.
	 * (int)	id				The ID of the certificate.
	 * callback(err, result)	If there is an error, it will be contained in
	 *							err.  Otherwise, result will contain the status
	 *							of the provided certificate ID.
	 */
	this.getCollectStatus = function(id, callback, timeout = 0) {
		// The id must be a positive number.
		if (!(typeof(id) == 'number')) {
			throw new TypeError("The argument 'id' must be a number.");
		}
		if (isNaN(id) || (id <= 0)) {
			throw new TypeError("The argument 'id' must be a positive number.");
		}

		var params = {
			authData: this.authData,
			id: id
		};

		SOAP.createClient(WSDL, function(err, client) {
			client.getCollectStatus(params, function(err, result, raw, soapHeader) {
				if (err) {
					callback(err, null);
				} else {
					var returnValue = result['return'];
					callback(null, returnValue);
				}
			}, {timeout: timeout});
		});
	}

	/**
	 * Calls the collect SOAP command.
	 * This command downloads the certificate identified by the provided id.
	 * (int)	id				The ID of the certificate.
	 * (int)	format			The format of the certificate.
	 * callback(err, result)	If there is an error, it will be contained in
	 *							err.  Otherwise, result will contain requested
	 *							certificate.
	 */
	this.collect = function(id, format, callback, timeout = 0) {
		// The id must be a positive number.
		if (!(typeof(id) == 'number')) {
			throw new TypeError("The argument 'id' must be a number.");
		}
		if (isNaN(id) || (id <= 0)) {
			throw new TypeError("The argument 'id' must be a positive number.");
		}
		// The format must be a positive number.
		if (!(typeof(format) == 'number')) {
			throw new TypeError("The argument 'format' must be a number.");
		}
		if (isNaN(format) || (!formatTypesMap.has(format))) {
			throw new TypeError("The argument 'format' must be a valid Format Type.");
		}

		var params = {
			authData: this.authData,
			id: id,
			formatType: format
		};

		SOAP.createClient(WSDL, function(err, client) {
			client.collect(params, function(err, result, raw, soapHeader) {
				if (err) {
					callback(err, null);
				} else {
					console.log(result);
					var returnValue = result['return'];
					callback(null, returnValue);
				}
			}, {timeout: timeout});
		});
	}
}
