var sinon = require('sinon');
var soapStub = require('soap/soap-stub');

const WSDL = 'https://cert-manager.com/ws/EPKIManagerSSL?wsdl';
var clientStub = {
    getCustomerCertTypesByOrg: sinon.stub()
}

clientStub.getCustomerCertTypes.respondWithError = soapStub.createErrorStub({});
clientStub.getCustomerCertTypes´.respondWithSuccess = soapStub.createRespondingStub([ { id: 224, name: 'InCommon SSL (SHA-2)', terms: [ 1, 2, 3 ] } ]);

soapStub.registerClient('testClient', WSDL, clientStub);
